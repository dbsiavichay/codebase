//Spliter
var splitVertical = null;
var splitHorizontal = null;

var activateButtonActions = function () {
	document.querySelectorAll('.editor-btn').forEach(function (item, index) {
		item.addEventListener('click', function (e) {				
			var target = this.getAttribute('target');
			if (target) activatePanel(target);
			else {
				showEditors();
				createSplitVertical();
			}
			activateButton(item);								
		});
	});
}

var showEditors = function () {
	document.querySelectorAll('.editor').forEach(function (item, index) {
		item.classList.remove('hidden');				
	});
}

var activateButton = function (button) {
	document.querySelectorAll('.editor-btn').forEach(function (e, i) {
		e.classList.remove('active');
	})

	button.classList.add('active');
}

var activatePanel = function (target) {
	var panel = document.querySelector(target);
	if (!panel) return;				
	destroySplitVertical();			
	showEditors();
	document.querySelectorAll('.editor:not('+ target +')').forEach(function (item, index) {
		item.classList.add('hidden');
	});	
		
	panel.setAttribute('style', 'height: 100%;');

}

var createSplitHorizontal = function () {
	splitHorizontal = Split(['#code', '#page'], {
		gutterSize: 4,
		direction: 'horizontal',
	});				
}

var createSplitVertical = function () {
	if (splitVertical) return;
	splitVertical  = Split(['#editorHtml', '#editorCss', '#editorJs'], {
		gutterSize: 2,
		direction: 'vertical',
	});
}

var destroySplitVertical = function () {
	if (!splitVertical) return;
	splitVertical.destroy();
	splitVertical = null;
}


document.addEventListener('DOMContentLoaded', function() {
	activateButtonActions();	
	createSplitHorizontal();
	createSplitVertical();
});