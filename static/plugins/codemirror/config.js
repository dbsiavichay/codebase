var htmlEditor, cssEditor, jsEditor;

//Editores
var initHtmlEditor = function () {
	var htmlTextarea = document.getElementById('id_html');
	htmlEditor = CodeMirror.fromTextArea(
		htmlTextarea,
		{
			lineNumbers: true,
			mode: 'htmlmixed',
		}
	);

	htmlEditor.on('change', function (editor, event) {
		event['editor'] = 'htmlEditor';		
		htmlTextarea.value = editor.getValue();
		if (typeof(commit) != 'undefined') commit(event, {html: editor.getValue()});								
		setTimeout(renderPreview, 300);
	});

	htmlEditor.on('keydown', function (event) {
		send = true;
	});	

	emmetCodeMirror(htmlEditor);
}

var initCssEditor = function () {
	var cssTextarea = document.getElementById('id_css');
	cssEditor = CodeMirror.fromTextArea(
		cssTextarea,
		{
			lineNumbers: true,
			mode: 'css',
		}
	);

	cssEditor.on('change', function (editor, event) {
		event['editor'] = 'cssEditor';
		cssTextarea.value = editor.getValue();	
		if (typeof(commit) != 'undefined') commit(event, {css: editor.getValue()});
		setTimeout(renderPreview, 300);
	});	

	cssEditor.on('keydown', function (event) {
		send = true;
	});	
}

var initJsEditor = function () {
	var jsTextarea = document.getElementById('id_js');
	jsEditor = CodeMirror.fromTextArea(
		jsTextarea,
		{
			lineNumbers: true,
			mode: 'javascript',
		}
	);

	jsEditor.on('change', function (editor, event) {
		event['editor'] = 'jsEditor';
		jsTextarea.value = editor.getValue();
		if (typeof(commit) != 'undefined') commit(event, {js: editor.getValue()});							
		setTimeout(renderPreview, 300);
	});	

	jsEditor.on('keydown', function (event) {
		send = true;
	});		
}

var renderPreview = function () {
	var previewFrame = document.getElementById('preview');
	var preview =  previewFrame.contentDocument ||  previewFrame.contentWindow.document;
	preview.open();
	preview.write('<style type="text/css">' + cssEditor.getValue() + '<\/style>');
	preview.write(htmlEditor.getValue());
	preview.write('<script>' + jsEditor.getValue() + '<\/script>');
	preview.close();
	$('#preview').contents().find('a').click(function(event) { event.preventDefault(); });
}

document.addEventListener('DOMContentLoaded', function() {
	initHtmlEditor();
	initCssEditor();
	initJsEditor();
	renderPreview();	
});