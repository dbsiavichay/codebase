var interval = null;
var autosave = document.getElementById('autosave');

if (autosave) {
	autosave.addEventListener('change', function (e) {
		if (e.target.checked) setAuto();
		else cleanAuto();
	});	
}

var setAuto = function () {
	interval = setInterval(function () {
		saveCodeForm();	
	}, 5000);
}

var cleanAuto = function () {
	clearInterval(interval);
}