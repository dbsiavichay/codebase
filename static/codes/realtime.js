var socket;
var state = false;
var send = false;

document.addEventListener('DOMContentLoaded', function() {
	init();		
});

var connectIO = function (room, sender) {		
	socket = io('//' + window.location.hostname + ':' + 3000, {query: 'room=' + room+'&sender='+ sender});

	socket.on('codebase', function (event) {
		send = false;					
		var editor = window[event['editor']];
		if (editor) editor.replaceRange(event.text, event.from, event.to);		
	});

	socket.on('init', function (data) {	
		send = false;										
		if(htmlEditor) htmlEditor.setValue(data.html);		
		if(cssEditor) cssEditor.setValue(data.css);		
		if(jsEditor) jsEditor.setValue(data.js);

		var elem = document.getElementById('collaborate');
		if (elem) elem.checked = data['mode'];		
	});	
}

var commit = function (event, data) {
	if(!send) return;		
	socket.emit('commit', event, data);
}

var collaborate = function (state) {
	if (!socket) return;
	var data = {}; 
	if(state) {

		data['html'] = htmlEditor.getValue();
		data['css'] = cssEditor.getValue();
		data['js'] = jsEditor.getValue();
	}
	socket.emit('collaborate', state, data);
}

var init = function () {
	var room = document.querySelector('input[name=room]').value;
	var sender = document.getElementById('user').value;	
	connectIO(room, sender);

	var elem = document.getElementById('collaborate');
	if (elem) {
		elem.addEventListener('change', function (e) {
			if (e.target.checked) {
				var modal = document.getElementById('collaborateModal');	
				if(modal) $('#collaborateModal').modal('show');				
			}
			collaborate(e.target.checked);
		});
	}
}