var btnSaveCode = document.getElementById('btnSaveCode');
var projectForm = document.getElementById('projectForm');
var btnForkCode = document.getElementById('btnForkCode');

if (btnSaveCode) {
	btnSaveCode.addEventListener('click', function (e) {
		e.preventDefault();
		var modal = document.getElementById('addCodeModal');	
		if(modal) $('#addCodeModal').modal('show');
		else saveCodeForm();
	});	
}

if (projectForm) {
	projectForm.addEventListener('submit', function (e) {	
		e.preventDefault();
		var name = document.querySelector('input[name=project]').value;
		document.getElementById('id_name').value = name;
		document.getElementById('saveCodeForm').submit();
		return false;
	});		
}

if (btnForkCode) {
	btnForkCode.addEventListener('click', function (e) {
		e.preventDefault();
		var form = document.getElementById('saveCodeForm');
		form.setAttribute('action', this.getAttribute('action'));
		form.submit();
	});	
}

var saveCodeForm = function () {
	var form = document.getElementById('saveCodeForm');
	var data = formToJson(form);
	$.post(form.action, data)
		.done(function (data) {
			console.log('Se ha guardado con éxito.');
		})
		.error(function (err) {			
			console.log(err);
		});
}


var formToJson = function (form) {
	var obj = {};
	var elements = form.querySelectorAll('input, select, textarea');
	for (var i = 0; i < elements.length; i++) {
		var element = elements[i];
		var name = element.name;
		var value = element.value;

		if (name) obj[name] = value;		
	}

	return obj;
}