const config = require('./config');
const pg = require('pg');
const connectionString = 'postgres://'+config.user+':'+config.password+'@'+config.host+':'+config.port+'/codebase';
const client = new pg.Client(connectionString);

//client.connect();

module.exports = {
	client: client,	
	connect: () => {
		client.connect();
	},
}