const db = require('./db');
const pg = require('pg');

const get_sql = 'SELECT * FROM codes_code WHERE room = $1;';

db.connect();

var get = (room) => {
	return new Promise(function (resolve, reject) {
		db.client.query(get_sql, [room,], (err, res) => {		
			if (err) {
				console.log(err.stack) 			
				return;			
			}
			if (res.rows.length == 1) resolve(res.rows[0]);
			else return null;
		});		
	});
}

module.exports = {
	get: get,	
}