var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var rooms = {}

var Code =  require('./db/code');

io.on('connection', socket => {		
	var room = socket.handshake.query.room;	
	socket.join(room);	
	
	socket.on('commit', (event, data) => {
		var sender = socket.handshake.query.sender;
		var owner = rooms[room].owner;
		var mode =  rooms[room].mode;

		if (!mode && (sender != owner)) return;
		if (mode) socket.broadcast.to(room).emit('codebase', event);
			
		if ('html' in data) rooms[room].html = data.html;
		if ('css' in data) rooms[room].css = data.css;
		if ('js' in data) rooms[room].js = data.js;		
	});

	socket.on('collaborate', (state, data) => {
		var sender = socket.handshake.query.sender;
		var owner = rooms[room].owner;
		if (sender == owner) {
			if (state) {
				rooms[room].html = data.html;
				rooms[room].css = data.css;
				rooms[room].js = data.js;				
			}
			rooms[room].mode = state;
			socket.broadcast.to(room).emit('init', rooms[room]);
		}
	});

	io.in(room).clients((err, clients) => {		
		if (clients.length > 1) {			
			if(rooms[room].mode) socket.emit('init', rooms[room]);
		} else {
			rooms[room] = {mode: false,};			
			Code.get(room).then(data => {				
				rooms[room].html = data.html;
				rooms[room].css = data.css;
				rooms[room].js = data.js;
				rooms[room].owner = data.user_id;								
			});
		}
		console.log('Socket join in room ' + room+ ':' + clients.length + ' users.');		
	});

});

http.listen(3000, () => {
	console.log('listening on *:3000');
});