from django.urls import path
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import logout_then_login, PasswordChangeView, PasswordChangeDoneView
from .views import (
	UserCreationView, LoginView,
	ProfileUpdateView,
	SocialLoginView
)

urlpatterns = [
	path('register/', UserCreationView.as_view(), name='register'),	
	path('login/', LoginView.as_view(), name='login'),
	path('logout/', login_required(logout_then_login), name='logout'),
	path('profile/update/', login_required(ProfileUpdateView.as_view()), name='update_profile'),	
	path(
		'change-password/', PasswordChangeView.as_view(
			template_name = 'authentication/change_password.html',
			success_url = '/authentication/change-password-done/'
		), 		
		name='change_password'
	),
	path(
    	'change-password-done/', PasswordChangeDoneView.as_view(
    		template_name = 'authentication/change_password_done.html',
    	), 
    ),
	
	path('social-login/', SocialLoginView.as_view(), name='social_login'),
]
