from django.test import TestCase
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.contrib.auth.models import User
from .models import Profile, SocialProfile

class ProfileTestCase(TestCase):
	def create_user(self):
		user = User.objects.create(username='test1')
		return user

	def test_create_user_with_profile_without_avatar(self):
		user = self.create_user()
		self.assertTrue(isinstance(user, User))
		self.assertTrue(isinstance(user.profile, Profile))
		self.assertEqual(user.profile.__str__(), user.username)
		self.assertIn('img/friends/avatar-bg.png', user.profile.get_avatar())

	def test_create_user_with_profile_with_avatar(self):
		user = self.create_user()
		profile = user.profile
		image_path = settings.BASE_DIR + '/static/img/img_test.jpg'
		profile.avatar = SimpleUploadedFile(name='img_test.jpg', content=open(image_path, 'rb').read(), content_type='image/jpeg')
		self.assertTrue(isinstance(profile, Profile))		
		self.assertIn('img_test.jpg', profile.get_avatar())	

class SocialProfileTestCase(TestCase):
	def setUp(self):
		User.objects.create(username='test1')

	def test_create_social_profile(self):
		user = User.objects.get(username='test1')
		social_profile = SocialProfile.objects.create(provider='facebook', identifier='1234567899', user=user)
		self.assertTrue(isinstance(social_profile, SocialProfile))
		self.assertEqual(social_profile.__str__(), user.username)
