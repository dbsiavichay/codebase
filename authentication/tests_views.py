from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User

class UserCreationViewTest(TestCase):
	def test_create_user(self):
		data = {'email':'test@test.com','username': 'test', 'password': 'test1234'}
		response = self.client.get(reverse('register'))
		self.client.post(reverse('register'), data)
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'authentication/register.html')
		self.assertEqual(User.objects.last().username, 'test')

class ProfileUpdateView(TestCase):
	def setUp(self):
		data = {'email':'test@test.com','username': 'test', 'password': 'test1234'}		
		self.client.post(reverse('register'), data)

	def test_update_existing_profile(self):
		data = {'email':'test@test.com','username': 'test', 'first_name': 'Test', 'last_name': 'Case'}		
		response = self.client.get(reverse('update_profile'))
		self.client.post(reverse('update_profile'), data)
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'authentication/profile_form.html')
		self.assertEqual(User.objects.last().get_full_name(), 'Test Case')