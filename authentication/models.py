from django.conf import settings
from django.db import models
from django.urls import reverse

class Profile(models.Model):
	class Meta:
		verbose_name = 'perfil'
		verbose_name_plural = 'perfiles'

	avatar = models.ImageField(upload_to='users/avatares/', blank=True, null=True, verbose_name='foto de perfil')
	user = models.OneToOneField('auth.User', on_delete=models.CASCADE, verbose_name='usuario')	

	def get_avatar(self):		
		if self.avatar:
			return self.avatar.url
		return settings.STATIC_URL + 'img/friends/avatar-bg.png'

	# def get_absolute_url(self):
	# 	return reverse('detail_profile', args=[self.user.username])

	def __str__(self):
		return self.user.get_full_name() or self.user.username

class SocialProfile(models.Model):
	class Meta:
		unique_together = ('provider','identifier')
		verbose_name = 'perfil social'
		verbose_name_plural = 'perfiles sociales'

	provider = models.CharField(max_length=16, verbose_name='proveedor')
	identifier = models.CharField(max_length=64, verbose_name='id usuario')
	user = models.ForeignKey('auth.User', on_delete=models.CASCADE, verbose_name='usuario')

	def __str__(self):
		return self.user.get_full_name() or self.user.username
