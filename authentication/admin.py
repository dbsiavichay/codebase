from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User, Group
from .models import Profile, SocialProfile

admin.site.site_header = 'Administración de Codebase'

class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = 'Perfil' 

class SocialProfileInline(admin.TabularInline):
	model = SocialProfile	
	list_display = ('provider', 'identifier',)
	verbose_name_plural = 'Cuentas sociales'

class CustomUserAdmin(UserAdmin):	
	fieldsets = (        
		(None, {
			'fields': ('username','password')
		}),
        ('Información personal', {            
            'fields': ('first_name','last_name', 'email')
        }),
        ('Permisos', {
        	'fields': ('is_active', 'is_staff', 'is_superuser')
        }),
        ('Fechas importantes', {
        	'fields': ('date_joined', 'last_login')
        })
    )	

	inlines =[
    	ProfileInline,
    	SocialProfileInline,
    ]

	def get_inline_instances(self, request, obj=None):
		if not obj:
			return list()
		return super(CustomUserAdmin, self).get_inline_instances(request, obj)



admin.site.unregister(Group)
admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)