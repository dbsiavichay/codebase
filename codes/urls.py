from django.urls import path
from django.contrib.auth.decorators import login_required
from authentication.decorators import owner_required
from .views import UserCodeListView, CodeCreateView, CodeUpdateView, CodeForkView

urlpatterns = [
	path('', login_required(UserCodeListView.as_view()), name='user_codes'),
	path('code/add/', CodeCreateView.as_view(), name='add_code'),
	path('code/fork/', CodeForkView.as_view(), name='fork_code'),
	path('code/<slug:room>/update/', CodeUpdateView.as_view(), name='update_code'),
]