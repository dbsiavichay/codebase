from django import forms
from django.forms.widgets import HiddenInput, Textarea
from .models import Code

class CodeForm(forms.ModelForm):
    class Meta:
        model = Code
        exclude = ('user',)
        widgets = {
        	'name': HiddenInput,
            'html': Textarea,
            'css': Textarea,
            'js': Textarea,
            'room': HiddenInput,
        }