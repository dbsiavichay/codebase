from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView
from .models import Code, Editor
from .forms import CodeForm

class UserCodeListView(ListView):
	model = Code
	template_name = 'codes/usercode_list.html'

	def get_queryset(self):
		queryset = super().get_queryset()
		return queryset.filter(user=self.request.user)

class CodeCreateView(CreateView):
	model = Code
	form_class = CodeForm

	def form_valid(self, form):
		self.object = form.save(commit=False)
		self.object.user = self.request.user
		self.object.save()
		return redirect(reverse_lazy('update_code', args=[self.object.room]))

	def post(self, request, *args, **kwargs):
		if not request.user.is_authenticated:
			return redirect('login')
		return super().post(request, *args, **kwargs)

class CodeUpdateView(UpdateView):
	model = Code
	form_class = CodeForm
	slug_field = 'room'
	slug_url_kwarg = 'room'

	def post(self, request, *args, **kwargs):
		self.object = self.get_object()
		if not request.user.is_authenticated or request.user != self.object.user:
			return redirect('login')
		return super().post(request, *args, **kwargs)

class CodeForkView(CreateView):
	model = Code
	fields = ['name', 'html', 'css', 'js']

	def form_valid(self, form):
		self.object = form.save(commit=False)
		self.object.user = self.request.user
		self.object.save()
		return redirect(reverse_lazy('update_code', args=[self.object.room]))

	def post(self, request, *args, **kwargs):
		if not request.user.is_authenticated:
			return redirect('login')
		return super().post(request, *args, **kwargs)

