from django.db import models
from django.urls import reverse_lazy
import uuid

class Code(models.Model):
	class Meta:
		verbose_name = 'código'
		
	name = models.CharField(max_length=64)
	html = models.TextField(blank=True, null=True, verbose_name='HTML')
	css = models.TextField(blank=True, null=True, verbose_name='CSS')
	js = models.TextField(blank=True, null=True, verbose_name='JavaScript')	
	room = models.UUIDField(default=uuid.uuid4, verbose_name='sala')	
	user = models.ForeignKey('auth.User', on_delete=models.CASCADE, verbose_name='usuario')

	def __str__(self):
		return self.name

	def get_absolute_url(self):
		return reverse_lazy('update_code', args=[self.room])

class Editor(models.Model):
	class Meta:
		unique_together = ('code', 'user')

	code = models.ForeignKey(Code, on_delete=models.CASCADE)
	user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
	is_owner = models.BooleanField(default=False)

	def __str__(self):
		return self.user.get_full_name() or self.user.username