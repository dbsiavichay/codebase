from django.apps import AppConfig


class CodesConfig(AppConfig):
    name = 'codes'
    verbose_name = 'Códigos'
